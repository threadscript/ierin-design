$("#contact_submit").validate({
    rules: {
        email: "required",
        contact: "required",
        name: "required",
        subject: "required",
        message: "required",
    },
    messages: {
        contact: "Please enter Contact details",
        email: "Please enter Email address",
        name: "Please enter Full Name",
        subject: "Please enter Subject",
        message: "Please enter Message",
    },
    highlight: function (label) {
        $(label).closest('.box-form-group').addClass('error');
    }
});

$("#subscribe_submit").validate({
    rules: {
        email: "required",
    },
    messages: {
        email: "Please enter Email address",
    },
    highlight: function (label) {
        $(label).closest('.box-form-group').addClass('error');
    }
});